<?php
    include_once('app/services/database.service.php');
    $database = new Database();

    $sql = "CREATE TABLE users (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(30) NOT NULL,
        user_name VARCHAR(30) NOT NULL,
        password VARCHAR(50),
        status CHAR(1),
        UNIQUE (user_name)
    )";

    if ($database->query($sql) === TRUE) {
        echo "Tabela de usuários criada com sucesso<br>";
    } else {
        echo "Erro ao criar a tabela de usuários: " . $database->error;
    }

    $password = md5('root');
    $sql = "INSERT INTO users VALUE (default, 'root', 'root', '$password', 1)";

    if ($database->query($sql) === TRUE) {
        echo "Usuário root incluído com sucesso: user_name = root, password = root<br>";
    } else {
        echo "Erro ao incluir usuário root: " . $database->error;
    }

    $sql = "CREATE TABLE companies (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(30) NOT NULL,
        status CHAR(1)
    )";

    if ($database->query($sql) === TRUE) {
        echo "Tabela de empresas criada com sucesso<br>";
    } else {
        echo "Erro ao criar a tabela de empresas: " . $database->error;
    }

    $sql = "CREATE TABLE users_companies (
        user_id INT(6) NOT NULL,
        company_id INT(6) NOT NULL,
        PRIMARY KEY (user_id, company_id)
    )";

    if ($database->query($sql) === TRUE) {
        echo "Tabela auxiliar de empresas criada com sucesso<br>";
    } else {
        echo "Erro ao criar a tabela auxiliar de empresas: " . $database->error;
    }
    
    $database->close();
