Com docker:
1) Executar o comando docker-compose up para criar os containers. Será cria uma tabela no mysql automaticamente chamada next.
2) Após o docker criar os containers, a porta 80 estará sendo utilizada para o apache escutar as requisições e a 3306 para o mysql.
3) Acessar a url http://localhost/seeder.php para que seja criada a estrutura base do banco de dados com um usuário inicial (user: root, senha: root).
4) Após o banco ser criado, basta acessar http://localhost e realizar o login com o usuário inicial.

Sem docker:
1) Será necessário ter instalado o pacote mysql, apache e php na máquina.
2) Basta copiar o projeto dentro da pasta html do apache e criar uma banco de dados chamado next.
3) Executar o script seeder.php para criar o usuário inicial (user: root, senha: root).
4) Executando o projeto sem o docker, será necessário alterar os dados de conexão com o banco.

Obs.: Os dados de conexão com o DB ficam no arquivo app/services/database.service.php.