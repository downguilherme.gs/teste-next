<?php
session_start();
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if ($_SERVER['HTTP_TOKEN'] != $_SESSION['token']) {
    header("HTTP/1.0 401 Unauthorized");
    exit;
}


include_once('../business/company.business.php');
$companyBusiness = new CompanyBusiness();

$id = $_GET['id'];
$response = $companyBusiness->get($id);

echo json_encode($response);