<?php
session_start();
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if ($_SERVER['HTTP_TOKEN'] != $_SESSION['token']) {
    header("HTTP/1.0 401 Unauthorized");
    exit;
}


include_once('../business/user.business.php');
$userBusiness = new UserBusiness();

$data = [
    "name"      => $_POST['name'],
    "user_name" => $_POST['login'],
    "password"  => $_POST['password'],
    "companies" => $_POST['companies'] ?? []
];
$response = $userBusiness->add($data);

echo json_encode($response);