<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once('../business/login.business.php');
$loginBusiness = new LoginBusiness();

$user = $_POST['user'];
$password = $_POST['password'];
$response = $loginBusiness->login($user, $password);

echo json_encode($response);
