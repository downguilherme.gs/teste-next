<?php
class Database extends mysqli
{  
    private $host = 'next-mysql';
    private $user = 'root';
    private $pass = 'root';
    private $db = 'next';

    public function __construct() {
        parent::__construct($this->host, $this->user, $this->pass, $this->db);

        if (mysqli_connect_error()) {
            die('Connect Error (' . mysqli_connect_errno() . ') ' .
                mysqli_connect_error());
        }
    }
}
?>