<?php
include_once('../model/company.php');

class CompanyBusiness 
{
    function __construct() {
        $this->companyModel = new Company(); 
    }

    function list($name) {
        return $this->companyModel->list($name);
    }

    function add($data) {
        return $this->companyModel->add($data);
    }

    function update($data, $id) {
        return $this->companyModel->update($data, $id);
    }

    function delete($id) {
        return $this->companyModel->delete($id);
    }

    function get($id) {
        return $this->companyModel->get($id);
    }
}
