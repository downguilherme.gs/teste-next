<?php
include_once('../model/user.php');

class UserBusiness 
{
    function __construct() {
        $this->userModel = new User(); 
    }

    function list($name) {
        return $this->userModel->list($name);
    }

    function add($data) {
        return $this->userModel->add($data);
    }

    function update($data, $id) {
        return $this->userModel->update($data, $id);
    }

    function delete($id) {
        return $this->userModel->delete($id);
    }

    function get($id) {
        return $this->userModel->get($id);
    }
}
