<?php
session_start();
include_once('../model/user.php');

class LoginBusiness 
{
    function __construct() {
        $this->userModel = new User(); 
    }

    function login($user, $password) {
        $response = $this->userModel->doLogin($user, $password);
        if (!$response) {
            return ["logged" => false];
        }
        $token = password_hash($user, PASSWORD_DEFAULT);
        $_SESSION['token'] = $token;
        return ["logged" => true, "token" => $token];
    }
}
