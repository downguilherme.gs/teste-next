<?php

include_once "../services/database.service.php";

class User extends Database
{
    private $conn;
    private $table = "users";
    private $fields = [
        "name",
        "user_name",
        "password",
        "status"
    ];
  
    public function __construct() {
        $this->conn = new Database();
    }

    public function doLogin($user, $password) {
        $password = md5($password);
        $sql = "SELECT * FROM {$this->table} WHERE user_name = '$user' AND password = '$password' AND status = 1 LIMIT 1";
        $result = $this->conn->query($sql);
        while($dados=mysqli_fetch_array($result)) {
            return $dados['id'];
        }
    }

    public function list($name) {
        $sql = "SELECT * FROM {$this->table} WHERE status = 1";
        if (!empty($name)) {
            $sql .= " AND name like '%$name%'";
        }
        $result = $this->conn->query($sql);
        $data = [];
        while($dados=mysqli_fetch_array($result)) {
            $data[] = $dados;
        }
        return $data;
    }

    public function add($data) {
        $data['status'] = 1;
        $data['password'] = md5($data['password']);
        $companies = $data['companies'];
        unset($data['companies']);
        $sql = "INSERT INTO {$this->table} (".implode(",", $this->fields).") VALUES ('".implode("','", $data)."')";
        if ($this->conn->query($sql) !== TRUE) {
            throw new Exception("Error: " . $this->conn->error);
        }
        $userId = $this->conn->insert_id;
        foreach($companies AS $companyId) {
            $sql = "INSERT INTO users_companies VALUES($userId, $companyId)";
            if ($this->conn->query($sql) !== TRUE) {
                throw new Exception("Error: " . $this->conn->error);
            }
        }
        return ["response" => "Usuário incluído com sucesso"];
    }

    public function update($data, $id) {
        $companies = $data['companies'];
        unset($data['companies']);
        if (!empty($data['password'])) {
            $data['password'] = md5($data['password']);
        } else {
            unset($data['password']);
        }
        $sql = "UPDATE {$this->table} SET ";
        $i = 0;
        foreach ($data AS $field=>$value) {
            if ($i > 0) {
                $sql .= ", ";
            }
            $sql .= "$field='$value'";
            $i++;
        }
        $sql .= " WHERE id = $id";
        if ($this->conn->query($sql) !== TRUE) {
            throw new Exception("Error: " . $this->conn->error);
        }

        $sql = "DELETE FROM users_companies WHERE user_id = $id";
        if ($this->conn->query($sql) !== TRUE) {
            throw new Exception("Error: " . $this->conn->error);
        }

        foreach($companies AS $companyId) {
            $sql = "INSERT INTO users_companies VALUES($id, $companyId)";
            if ($this->conn->query($sql) !== TRUE) {
                throw new Exception("Error: " . $this->conn->error);
            }
        }

        return ['response' => 'Usuário alterado com sucesso'];
    }

    public function delete($id) {
        $sql = "UPDATE {$this->table} SET status = 0 WHERE id = $id";
        if ($this->conn->query($sql) === TRUE) {
            return ["response" => "Usuário excluído com sucesso"];
        } else {
            throw new Exception("Error: " . $this->conn->error);
        }
    }

    public function get($id) {
        $sql = "SELECT * FROM {$this->table} WHERE id = " . $id;
        $result = $this->conn->query($sql);
        while($dados=mysqli_fetch_array($result)) {
            $user = $dados;
        }
        $sql = "SELECT * FROM users_companies WHERE user_id = " . $id;
        $result = $this->conn->query($sql);
        $user['companies'] = [];
        while($dados=mysqli_fetch_array($result)) {
            $user['companies'][] = $dados;
        }
        return $user;
    }
}
