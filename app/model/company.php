<?php

include_once "../services/database.service.php";

class Company extends Database
{
    private $conn;
    private $table = "companies";
    private $fields = [
        "name",
        "status"
    ];
  
    public function __construct() {
        $this->conn = new Database();
    }

    public function list($name) {
        $sql = "SELECT * FROM {$this->table} WHERE status = 1";
        if (!empty($name)) {
            $sql .= " AND name like '%$name%'";
        }
        $result = $this->conn->query($sql);
        $data = [];
        while($dados=mysqli_fetch_array($result)) {
            $data[] = $dados;
        }
        return $data;
    }

    public function add($data) {
        $data['status'] = 1;
        $sql = "INSERT INTO {$this->table} (".implode(",", $this->fields).") VALUES ('".implode("','", $data)."')";
        if ($this->conn->query($sql) === TRUE) {
            return ["response" => "Empresa incluída com sucesso"];
        } else {
            throw new Exception("Error: " . $this->conn->error);
        }
    }

    public function update($data, $id) {
        $sql = "UPDATE {$this->table} SET ";
        $i = 0;
        foreach ($data AS $field=>$value) {
            if ($i > 0) {
                $sql .= ", ";
            }
            $sql .= "$field='$value'";
            $i++;
        }
        $sql .= " WHERE id = $id";
        if ($this->conn->query($sql) === TRUE) {
            return ['response' => 'Empresa alterada com sucesso'];
        } else {
            throw new Exception("Error: " . $this->conn->error);
        }
    }

    public function delete($id) {
        $sql = "UPDATE {$this->table} SET status = 0 WHERE id = $id";
        if ($this->conn->query($sql) === TRUE) {
            return ["response" => "Empresa excluída com sucesso"];
        } else {
            throw new Exception("Error: " . $this->conn->error);
        }
    }

    public function get($id) {
        $sql = "SELECT * FROM {$this->table} WHERE id = " . $id;
        $result = $this->conn->query($sql);
        while($dados=mysqli_fetch_array($result)) {
            return $dados;
        }
    }
}
