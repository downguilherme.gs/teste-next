$(function(){
    let pathname = window.location.pathname
    switch(pathname) {
        case "/resources/html/pages/company.html":
        case "/resources/html/pages/company.management.html":
            $("#company-menu").addClass("active");
            break;
        case "/resources/html/pages/user.html":
        case "/resources/html/pages/user.management.html":
            $("#user-menu").addClass("active");
            break;
    }

    $("#logout").on("click", function() {
        $.ajax({
            type: 'POST',
            url: '/app/controller/logout.controller.php',
            async: true,
            success: function() {
                localStorage.removeItem("next-token");
                window.location.href = "login.html";
            },
            error: function(err) {
                alert("Ocorreu um erro ao realizar o logout");
                console.log(err);
            }
        });
    })
});
