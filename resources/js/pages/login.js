$(function(){
    $("#login").on("click", function() {
        if (!validateFields()) {
            return;
        }

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/app/controller/login.controller.php',
            async: true,
            data: {
                user: $("#user").val(),
                password: $("#password").val()
            },
            success: function(response) {
                if (!response.logged) {
                    alert("Usuário e/ou senha inválidos");
                    return;
                }
                localStorage.setItem("next-token", response.token);
                window.location.href = "user.html";
            }
        });
    });

    validateFields = function() {
        let user = $("#user").val();
        if (user.trim() == "") {
            alert("Usuário não preenchido");
            return false;
        }
        let password = $("#password").val();
        if (password.trim() == "") {
            alert("Senha não preenchida");
            return false;
        }
        return true;
    }
});