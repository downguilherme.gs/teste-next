$(function(){
    $("#menu").load("../menu.html");
    $("#user-menu").addClass("active"); 
    search = function() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/app/controller/user.list.controller.php',
            async: true,
            data: {
                name: $("#name").val().trim()
            },
            headers: {
                "token": localStorage.getItem("next-token")
            },
            success: function(response) {
                fillTable(response);
            },
            error: function(err) {
                alert("Ocorreu um erro ao buscar os usuários");
                console.log(err);
            },
            statusCode: {
                401: function() {
                    window.location.href = "login.html";
                }
            }
        });
    }

    search();

    $("#search").on("click", function() {
        search();
    });

    $("#new-user").on("click", function() {
        window.location.href = "user.management.html";
    });

    fillTable = function(data) {
        $(".table tbody").empty();
        if (data.length == 0) {
            $(".table tbody").append(
                "<tr>"+
                    "<td colspan='4'>Nenhum usuário encontrado com esse nome</th>"+
                "</tr>"
            );
        }
        for(let i in data) {
            $(".table tbody").append(
                "<tr>"+
                    "<td width='10%'>"+data[i].id+"</th>"+
                    "<td width='40%'>"+data[i].name+"</td>"+
                    "<td width='30%'>"+data[i].user_name+"</td>"+
                    "<td width='20%'>"+
                    "<button type='button' class='btn btn-warning' onclick=\"editUser("+data[i].id+")\" style='margin-right: 10px;'>Editar</button>"+
                    "<button type='button' class='btn btn-danger' onclick=\"deleteUser("+data[i].id+")\">Excluir</button>"+
                    "</td>"+
                "</tr>"
            );
        }
    }

    editUser = function(id) {
        window.location.href = "user.management.html?id="+id;
    }

    deleteUser = function(id) {
        if (!confirm("Deseja realmente excluir o usuário?")) {
            return
        }
        $.ajax({
            type: 'DELETE',
            url: '/app/controller/user.delete.controller.php?id='+id,
            async: true,
            headers: {
                "token": localStorage.getItem("next-token")
            },
            success: function(response) {
                alert("Usuário excluído com sucesso");
                $( "tbody tr" ).each(function( index, row ) {
                    if ($(row).find("td").eq(0).html() == id) {
                        $(row).remove()
                    }
                });
            },
            error: function(err) {
                alert("Ocorreu um erro ao excluir o usuário");
                console.log(err);
            },
            statusCode: {
                401: function() {
                    window.location.href = "login.html";
                }
            }
        });
    }
});