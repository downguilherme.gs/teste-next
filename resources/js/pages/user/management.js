$(function(){
    $("#menu").load("../menu.html");
    $("#user-menu").addClass("active"); 
    $("#cancel").on("click", function() {
        window.location.href = "user.html";
    })

    $.ajax({
        type: 'GET',
        url: '/app/controller/company.list.controller.php',
        async: true,
        headers: {
            "token": localStorage.getItem("next-token")
        },
        success: function(response) {
            $(response).each(function (key, company) {
                $("#companies").append(
                    '<option value="'+company.id+'">'+company.name+'</option>'
                );
            });
        },
        error: function(err) {
            alert("Ocorreu um erro ao buscar as empresas");
            console.log(err);
        },
        statusCode: {
            401: function() {
                window.location.href = "login.html";
            }
        }
    });

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    var id = urlParams.get('id');
    if (id) {
        $.ajax({
            type: 'GET',
            url: '/app/controller/user.get.controller.php?id='+id,
            async: true,
            headers: {
                "token": localStorage.getItem("next-token")
            },
            success: function(response) {
                if (!response) {
                    alert("Ocorreu um erro ao buscar o usuário");
                    window.location.href = "user.html";
                }
                $("#name").val(response['name']);
                $("#user_name").val(response['user_name']);
                $("#password").val("");
                let companies = [];
                for (let key in response['companies']) {
                    companies.push(response['companies'][key].company_id);
                }
                $("#companies").val(companies);
            },
            error: function(err) {
                alert("Ocorreu um erro ao buscar o usuário");
                console.log(err);
            }
        });
    }

    $("#save").on("click", function() {
        if (!validateFields()) {
            return
        }
        url = '/app/controller/user.add.controller.php';
        if (id) {
            url = '/app/controller/user.update.controller.php?id='+id;
        }

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            async: true,
            data: {
                name: $("#name").val().trim(),
                login: $("#user_name").val().trim(),
                password: $("#password").val().trim(),
                companies: $("#companies").val()
            },
            headers: {
                "token": localStorage.getItem("next-token")
            },
            success: function(response) {
                alert("Usuário salvo com sucesso.");
                window.location.href = "user.html";
            },
            error: function(err) {
                alert("Ocorreu um erro ao salvar o usuário");
                console.log(err);
            }
        });
    })

    validateFields = function() {
        if ($("#name").val().trim() == "") {
            alert("O campo nome deve ser preenchido");
            return false;
        }

        if ($("#user_name").val().trim() == "") {
            alert("O campo login deve ser preenchido");
            return false;
        }

        if (!id && $("#password").val().trim() == "") {
            alert("O campo senha deve ser preenchido");
            return false;
        }

        return true;
    }
});