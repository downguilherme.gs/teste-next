$(function(){
    $("#menu").load("../menu.html");
    $("#company-menu").addClass("active"); 
    search = function() {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/app/controller/company.list.controller.php',
            async: true,
            data: {
                name: $("#name").val().trim()
            },
            headers: {
                "token": localStorage.getItem("next-token")
            },
            success: function(response) {
                fillTable(response);
            },
            error: function(err) {
                alert("Ocorreu um erro ao buscar as empresas");
                console.log(err);
            },
            statusCode: {
                401: function() {
                    window.location.href = "login.html";
                }
            }
        });
    }

    search();

    $("#search").on("click", function() {
        search();
    });

    $("#new-company").on("click", function() {
        window.location.href = "company.management.html";
    });

    fillTable = function(data) {
        $(".table tbody").empty();
        if (data.length == 0) {
            $(".table tbody").append(
                "<tr>"+
                    "<td colspan='3'>Nenhuma empresa encontrada com esse nome</th>"+
                "</tr>"
            );
        }
        for(let i in data) {
            $(".table tbody").append(
                "<tr>"+
                    "<td width='10%'>"+data[i].id+"</th>"+
                    "<td width='40%'>"+data[i].name+"</td>"+
                    "<td width='20%'>"+
                        "<button type='button' class='btn btn-warning' onclick=\"editCompany("+data[i].id+")\" style='margin-right: 10px;'>Editar</button>"+
                        "<button type='button' class='btn btn-danger' onclick=\"deleteCompany("+data[i].id+")\">Excluir</button>"+
                    "</td>"+
                "</tr>"
            );
        }
    }

    editCompany = function(id) {
        window.location.href = "company.management.html?id="+id;
    }

    deleteCompany = function(id) {
        if (!confirm("Deseja realmente excluir a empresa?")) {
            return
        }
        $.ajax({
            type: 'DELETE',
            url: '/app/controller/company.delete.controller.php?id='+id,
            async: true,
            headers: {
                "token": localStorage.getItem("next-token")
            },
            success: function(response) {
                alert("Empresa excluída com sucesso");
                $( "tbody tr" ).each(function( index, row ) {
                    if ($(row).find("td").eq(0).html() == id) {
                        $(row).remove()
                    }
                });
            },
            error: function(err) {
                alert("Ocorreu um erro ao excluir a empresa");
                console.log(err);
            },
            statusCode: {
                401: function() {
                    window.location.href = "login.html";
                }
            }
        });
    }
});