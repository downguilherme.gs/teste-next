$(function(){
    $("#menu").load("../menu.html");
    $("#company-menu").addClass("active"); 
    $("#cancel").on("click", function() {
        window.location.href = "company.html";
    })

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    var id = urlParams.get('id');
    if (id) {
        $.ajax({
            type: 'GET',
            url: '/app/controller/company.get.controller.php?id='+id,
            async: true,
            headers: {
                "token": localStorage.getItem("next-token")
            },
            success: function(response) {
                console.log(response)
                if (!response) {
                    alert("Ocorreu um erro ao buscar a empresa");
                    window.location.href = "company.html";
                }
                $("#name").val(response['name']);
            },
            error: function(err) {
                alert("Ocorreu um erro ao buscar a empresa");
                console.log(err);
            },
            statusCode: {
                401: function() {
                    window.location.href = "login.html";
                }
            }
        });
    }

    $("#save").on("click", function() {
        if (!validateFields()) {
            return
        }
        url = '/app/controller/company.add.controller.php';
        if (id) {
            url = '/app/controller/company.update.controller.php?id='+id;
        }
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            async: true,
            data: {
                name: $("#name").val().trim()
            },
            headers: {
                "token": localStorage.getItem("next-token")
            },
            success: function(response) {
                alert("Empresa salva com sucesso.");
                window.location.href = "company.html";
            },
            error: function(err) {
                alert("Ocorreu um erro ao salvar a empresa");
                console.log(err);
            },
            statusCode: {
                401: function() {
                    window.location.href = "login.html";
                }
            }
        });
    })

    validateFields = function() {
        if ($("#name").val().trim() == "") {
            alert("O campo nome deve ser preenchido");
            return false;
        }

        return true;
    }
});